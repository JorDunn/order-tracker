import pytest
from test_app import client, runner


def test_order_feed(client):
    assert client.get('/').status_code == 200


def test_order_new(client):
    assert client.get('/order/new').status_code == 200


@pytest.mark.parametrize(
    ('order_id', 'message'),
    (('OT78342', 'Order Successfully Retrieved'),
     ('OT39275', 'Order Does Not Exist'),
     ('', 'Order Number Is Required'),
     ))
def test_order_view(client, order_id, message):
    assert client.get('/order/<order_id>').status_code == 200


@pytest.mark.parametrize(
    ('order_id', 'customer_id', 'order_status', 'message'),
    (('OT78342', '1192453', '1', 'Order Successfully Updated'),
     ('OT39275', '1192513', '2', 'Order Does Not Exist'),
     ('', '3852450', '2', 'Order Number Is Required'),
     ('OT29571', '', '1', 'Customer Number Is Required'),
     ('OT18524', '1942763', '', 'Order Status Is Required'),
     ))
def test_order_update(client, order_id, customer_id, order_status, message):
    assert client.get('/order/<order_id>/update').status_code == 200
    # Insert post data
    assert client.post('/order/<order_id>/update').status_code == 302


@pytest.mark.parametrize(
    ('order_id', 'message'),
    (('OT78342', 'Order Successfully Deleted'),
     ('OT39275', 'Order Does Not Exist'),
     ('', 'Order Number Is Required'),
     ))
def test_order_delete(client, order_id, message):
    # Insert post data
    assert client.post('/order/<order_id>/delete', data={'order_id': order_id}).status_code == 302


def test_user_auth_login(client):
    assert client.get('/login').status_code == 200


def test_user_auth_logout(client):
    assert client.get('/logout').status_code == 302


def test_user_auth_register(client):
    assert client.get('/register').status_code == 200
