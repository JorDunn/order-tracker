from flask import render_template, Blueprint, current_app, redirect, request
from order_tracker.extentions import login_manager
from order_tracker.models import User
from flask_login import login_required, login_user, logout_user, current_user

orders = Blueprint('orders', __name__)


@login_manager.user_loader
def load_user(user_id):
    return User.get_id(user_id)


login_manager.login_view = '/login'


@orders.route('/', methods=['GET'])
@login_required
def order_feed():
    return render_template('index.html', page={'title': 'Order Feed'})


@orders.route('/order/new', methods=['GET', 'POST'])
@login_required
def order_new():
    return render_template('orders/new.html', page={'title': 'New Order'})


@orders.route('/order/<order_id>', methods=['GET'])
@login_required
def order_view(order_id):
    return render_template('orders/view.html', page={'title': 'Order ' + order_id})


@orders.route('/order/<order_id>/update', methods=['GET', 'POST'])
@login_required
def order_update(order_id):
    if request.method == 'GET':
        return render_template('orders/update.html', page={'title': 'Update Order' + order_id})
    elif request.method == 'POST':
        return redirect('/orders/' + order_id + '/view')


@orders.route('/order/<order_id>/delete', methods=['POST'])
@login_required
def order_delete(order_id):
    return redirect('/orders')


user_auth = Blueprint('user_auth', __name__)


@user_auth.route('/login', methods=['GET'])
def login():
    return render_template('login.html', page={'title': 'Login'})


@user_auth.route('/logout', methods=['GET'])
@login_required
def logout():
    return redirect('/login')


@user_auth.route('/register', methods=['GET', 'POST'])
def register():
    return render_template('register.html', page={'title': 'Register'})
