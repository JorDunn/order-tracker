# Order Tracker


[![pipeline status](https://gitlab.com/JorDunn/order-tracker/badges/master/pipeline.svg)](https://gitlab.com/JorDunn/order-tracker/commits/master)
[![coverage report](https://gitlab.com/JorDunn/order-tracker/badges/master/coverage.svg)](https://gitlab.com/JorDunn/order-tracker/commits/master)

Just a simple order tracking system. Nothing more, nothing less.