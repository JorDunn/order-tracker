class Config(object):
    DEBUG = True
    TESTING = True
    ENV = 'development'
    SECRET_KEY = 'changeme'
    PONY = {
        'provider': 'postgres',
        'user': 'order_tracker',
        'password': '',
        'host': 'postgres',
        'database': 'order_tracker'
    }
