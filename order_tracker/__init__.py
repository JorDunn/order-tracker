import os
from flask import Flask
from pony.orm import BindingError
from order_tracker.extentions import login_manager, db


def create_app():
    app = Flask(__name__, template_folder='views')
    app.config.from_object('order_tracker.default_settings.Config')
    if 'OTAPPLICATION_SETTINGS' in os.environ:
        app.config.from_envvar('OTAPPLICATION_SETTINGS')

    from order_tracker.routes import orders, user_auth
    app.register_blueprint(orders)
    app.register_blueprint(user_auth)

    login_manager.init_app(app)

    from pony.flask import Pony
    try:
        db.bind(**app.config['PONY'])
    except BindingError as e:
        print("BindingError: ", e)
    try:
        db.generate_mapping(create_tables=True)
    except BindingError as e:
        print("BindingError: ", e)
    Pony(app)

    return app
