import pytest

from order_tracker import create_app
from order_tracker.models import db


@pytest.fixture
def app():
    app = create_app()
    yield app


@pytest.fixture
def client():
    app = create_app()
    return app.test_client()


@pytest.fixture
def runner():
    app = create_app()
    return app.test_cli_runner()


@pytest.fixture(autouse=True)
def setup():
    db.disconnect()
