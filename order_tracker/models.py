from pony.orm import Required, Optional, Set
from flask_login import UserMixin
from datetime import datetime
from order_tracker.extentions import db


class User(db.Entity, UserMixin):
    username = Required(str, unique=True)
    password = Required(str)
    email = Required(str, unique=True)
    last_login_timestamp = Optional(datetime)
    passwd_reset_token = Optional(str)
    passwd_reset_timestamp = Optional(str)


class Customer(db.Entity):
    first_name = Required(str)
    last_name = Required(str)
    business_name = Optional(str)
    phone_number = Required(int)
    email_address = Optional(str)


class Order(db.Entity):
    customer_id = Required(int)
    status = Required(int)
    order_date = Required(datetime)
    lines = Set('OrderLine')
    notes = Required(str)


class OrderLine(db.Entity):
    order_id = Set('Order')
    quantity = Required(int)
    on_order = Required(int)
    released = Required(int)
    backordered = Required(int)
    part_number = Required(int)
