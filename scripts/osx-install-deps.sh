# Very rough dependency install script. Doesn't work yet.

# This should fix issues with psycopg2 complaining about missing -lssl when trying to install it.
export LDFLAGS="-L/usr/local/opt/openssl/lib"

brew update

if [`which postgres` == ""]; then
    brew install postgres
fi

if [`which python3.7` == ""]; then
    brew install python3
fi